using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{
    [Range(1.0f,5.0f)]
    [SerializeField]
    private float move_sensitivity=5.0f;

    [Range(1.0f, 5.0f)]
    [SerializeField]
    private float jump_force = 14.0f;

    private enum MovementState { idle, running, jumping, falling }



    private bool isGrounded;

    private Animator anim;
    private Rigidbody2D rb;
    private float moveInput = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
       
        moveInput = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveInput * move_sensitivity, rb.velocity.y);

        if (Input.GetButtonDown("Jump"))
        {
            rb.velocity = new Vector2(rb.velocity.x, jump_force);
        }

        UpdateAnimationState();



    }

    private void UpdateAnimationState()
    {
        MovementState state;

        if (moveInput > 0f)
        {
            state = MovementState.running;
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else if (moveInput < 0f)
        {
            state = MovementState.running;
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else
        {
            state = MovementState.idle;
        }

        if (rb.velocity.y > .1f)
        {
            state = MovementState.jumping;
        }
        else if (rb.velocity.y < -.1f)
        {
            state = MovementState.falling;
        }

        anim.SetInteger("state", (int)state);
    }
}
